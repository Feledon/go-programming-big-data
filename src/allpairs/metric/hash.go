package metric

import (
	"fmt"
	"image/color"
	"log"
	"path"
	"sync"

	"github.com/disintegration/imaging"
	"github.com/harrydb/go/img/grayscale"
)

const (
	hashWidth  = 8
	hashHeight = 8
	hashPixels = hashWidth * hashHeight
)

// Compute the Perceptual Hash of an Image.
func ComputePerceptualHash(path string) uint64 {
	// Open image for processing

	// Resize image to hashWidth x hashHeight using BSpline

	// Reduce color to grayscale using ToGrayLuminance

	// Compute the average of the color values

	// Compute the hash by setting the bits of a uint64 based on if color
	// value is above or below the mean.

	return 0
}

// Computer Perceptual Hashes for specified files
func ComputePerceptualHashesFromFiles(paths []string) {
}
