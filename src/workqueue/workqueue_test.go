package workqueue

import (
	"fmt"
	"os/exec"
	"strconv"
	"testing"
)

const NTASKS = 5
const WQNAME = "WorkQueueTest"

func TestWorkQueue(t *testing.T) {
	wq := NewWorkQueue(RANDOM_PORT)
	wq.SpecifyMode(CATALOG)
	wq.SpecifyName(WQNAME)

	if wq.Name() != WQNAME {
		t.Fatalf("wq.Name() is not %s", WQNAME)
	}

	for i := 0; i < NTASKS; i++ {
		command := fmt.Sprintf("uname -a > uname.%d", i)
		task := NewTask(command)
		wq.Submit(task)
	}

	cmd := exec.Command("work_queue_worker", "-t", "5", "localhost", strconv.Itoa(wq.Port()))
	cmd.Start()

	for !wq.Empty() {
		task := wq.Wait(WAIT_FOR_TASK)

		if task == nil {
			continue
		}

		if task.ExitCode() != 0 {
			t.Fatalf("task %s failed with exit code %d\n", task.Command(), task.ExitCode())
		}
		task.Delete()
	}
}
