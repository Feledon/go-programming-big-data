package metric

import (
	"fmt"
	"path"
)

// Compute the hamming distance between two Perceptual Hashes.
func ComputeHammingDistance(phash0, phash1 uint64) uint64 {
	return 0
}

// Print hamming distance of two images.
func PrintHammingDistance(path0, path1 string, hamming uint64) {
	fmt.Printf("%s %s = %2v (%3v%%)\n", path.Base(path0), path.Base(path1), hamming, (hashPixels-hamming)*100/hashPixels)
}

// Compute the hamming distances between two image files.
func ComputeHammingDistanceFromFiles(file0, file1 string) {
}
