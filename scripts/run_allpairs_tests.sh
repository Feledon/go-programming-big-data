#!/bin/sh

[ -z "${GOPATH}" ] && [ -r scripts/env.sh ] && . scripts/env.sh

set -e

PACKAGES="
    allpairs/utils
    allpairs/metric
    allpairs/serial
    allpairs/parallel
    allpairs/distributed
"

echo "Building allpairs..."
go install allpairs

for package in ${PACKAGES}; do
    echo "Testing ${package}..."
    go test -v ${package}
done
