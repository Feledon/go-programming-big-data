# usage:
#
#   $ source ./env.sh

if [ -z "$_" ]; then
    sourced_file=$BASH_SOURCE
else
    sourced_file=$_
fi

echo "     GOMAXPROCS is $GOMAXPROCS"

if [ -z "$sourced_file" ]; then
    GOPATH=$(pwd)
else
    GOPATH=$(dirname $(readlink -f $sourced_file))
fi
export GOPATH
echo "         GOPATH is $GOPATH"

WORK_QUEUE_NAME=allpairs-${USER}
export WORK_QUEUE_NAME
echo "WORK_QUEUE_NAME is $WORK_QUEUE_NAME"
