package utils

import "fmt"

func ExampleGenerateCombinations() {
	strings := []string{"huey", "dewey", "louie"}

	for pair := range GenerateCombinations(strings, false) {
		fmt.Printf("%v\n", pair)
	}

	for pair := range GenerateCombinations(strings, true) {
		fmt.Printf("%v\n", pair)
	}

	// Output:
	// {huey huey}
	// {huey dewey}
	// {huey louie}
	// {dewey dewey}
	// {dewey louie}
	// {louie louie}
	// {huey huey}
	// {huey dewey}
	// {huey louie}
	// {dewey huey}
	// {dewey dewey}
	// {dewey louie}
	// {louie huey}
	// {louie dewey}
	// {louie louie}
}

func ExampleGenerateChunks() {
	strings := []string{"0", "1", "2", "3", "4", "5", "6"}

	for chunk := range GenerateChunks(strings, 2) {
		fmt.Println(chunk)
	}

	// Output:
	// [0 1]
	// [2 3]
	// [4 5]
	// [6]
}
