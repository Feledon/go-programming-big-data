package metric

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"path"
	"sort"
	"strings"

	"allpairs/data"
)

func ExampleComputePerceptualHash() {
	for _, child := range data.GetChildrenPaths() {
		fmt.Printf("%v: %016x\n", path.Base(child), ComputePerceptualHash(child))
	}

	// Output:
	// caleb0.jpg: f9fbc08080c3dfff
	// caleb1.jpg: ffffe08080e0ece0
	// caleb2.jpg: ffffc080c0f0fcf4
	// caleb3.jpg: ffffc080c0e0fce4
	// twins0.jpg: f8f8c0c0e0e0f8fc
	// twins1.jpg: f8c0c0f0f0f0f8fc
	// twins2.jpg: fcf8e0e0c0f0fcfc
	// twins3.jpg: fcf8e0e0c0e0f8fc
}

func ExampleComputePerceptualHashesFromFiles() {
	// HACK: Capture stdout, Sort it, and then Print it
	//
	// This allows us to handle non-deterministic output.  Based on the following:
	//
	//	http://stackoverflow.com/questions/10473800/in-go-how-do-i-capture-stdout-of-a-function-into-a-string

	oldStdout := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w

	ComputePerceptualHashesFromFiles(data.GetChildrenPaths())

	output := make(chan string)
	go func() {
		var buffer bytes.Buffer
		io.Copy(&buffer, r)
		output <- buffer.String()
	}()
	w.Close()
	os.Stdout = oldStdout

	lines := strings.Split(<-output, "\n")
	sort.Strings(lines)
	for _, line := range lines {
		fmt.Println(line)
	}

	// Output:
	// caleb0.jpg 18013202792747098111
	// caleb1.jpg 18446709441255501024
	// caleb2.jpg 18446674257958206708
	// caleb3.jpg 18446674257957158116
	// twins0.jpg 17940301050269464828
	// twins1.jpg 17924538658001582332
	// twins2.jpg 18228566747696397564
	// twins3.jpg 18228566747695347964
}
