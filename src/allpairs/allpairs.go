package main

import (
	"flag"
	"fmt"
	"log"

	"time"

	"allpairs/distributed"
	"allpairs/metric"
	"allpairs/parallel"
	"allpairs/serial"
	"allpairs/utils"
)

const (
	DefaultAllPairs = true
	DefaultMethod   = "serial"
	DefaultTimed    = false
)

func main() {
	var (
		all    bool
		chunk  int
		timed  bool
		method string
	)

	flag.BoolVar(&all, "all", DefaultAllPairs, "Whether or not to process all pairs (even duplicates)")
	flag.IntVar(&chunk, "chunk", utils.DefaultChunkSize, "Size of distributed chunk")
	flag.BoolVar(&timed, "timed", DefaultTimed, "Whether or not to display the elapsed run-time")
	flag.StringVar(&method, "method", DefaultMethod, "Method to use (serial, parallel, distributed, distance, hash)")
	flag.Parse()

	if len(flag.Args()) == 0 {
		log.Fatalf("No files specified")
	}

	start_time := time.Now().UnixNano()

	if method == "serial" {
		serial.AllPairsSerial(flag.Args(), all)
	} else if method == "parallel" {
		parallel.AllPairsParallel(flag.Args(), all)
	} else if method == "distributed" {
		distributed.AllPairsDistributed(flag.Args(), all, chunk)
	} else if method == "distance" {
		if len(flag.Args()) != 2 {
			log.Fatalf("hamming method only takes two paths, given: %s", flag.Args())
		}
		metric.ComputeHammingDistanceFromFiles(flag.Args()[0], flag.Args()[1])
	} else if method == "hash" {
		metric.ComputePerceptualHashesFromFiles(flag.Args())
	} else {
		log.Fatalf("Unsupported method: %s", method)
	}

	end_time := time.Now().UnixNano()

	if timed {
		fmt.Printf("Elapsed time: %0.2f seconds\n", float64(end_time-start_time)/1000000000.0)
	}
}
